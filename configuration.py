# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'
    # Remove in future
    printing_taxes_receipt = fields.Boolean('Printing Taxes on Receipt')
    tip_rate = fields.Float('Tip Rate', digits=(2, 2))
    tip_product = fields.Many2One('product.product', 'Tip Product',
        domain=[
            ('template.salable', '=', True),
            ('template.type', '=', 'service'),
        ])
    send_sms_points = fields.Boolean('Send SMS Points')
    min_margin = fields.Float('Minimal Margin', digits=(4, 2))
    discount_pos_method = fields.Selection([
        ('percentage', 'Percentage'),
        ('fixed', 'Fixed'),
    ], 'Discount POS Method', required=True)
    show_position_pos = fields.Boolean('Show Position POS')
    show_discount_pos = fields.Boolean('Show Discount POS')
    show_delivery_charge = fields.Boolean('Show Delivery Charge')
    show_delivery_party = fields.Boolean('Show Delivery Party')
    show_agent_pos = fields.Boolean('Show Agent POS')
    check_credit_limit = fields.Boolean('Check Credit Limit')
    password_force_assign = fields.Char('Password Force Assign')
    password_admin_pos = fields.Char('Password Admin POS')
    encoded_sale_price = fields.Boolean('Encoded Sale Price')
    show_product_image = fields.Boolean('Show Product Image')
    new_sale_automatic = fields.Boolean('New Sale Automatic')
    show_description_pos = fields.Boolean('Show Description POS')
    show_party_categories = fields.Boolean('Show Party Categories')
    show_brand = fields.Boolean('Show Brand')
    show_stock_pos = fields.Selection([
        ('false', 'False'),
        ('icon', 'Icon'),
        ('value', 'Value'),
    ], 'Show Stock POS')
    show_order_number = fields.Boolean('Show Order Number')
    reconcile_invoices = fields.Boolean('Auto Reconcile Invoices')
    show_location_pos = fields.Boolean('Show Location POS')
    decimals_digits_quantity = fields.Integer('Decimals Digits of Quantity')
    show_fractions = fields.Boolean('Show Fractions')
    use_price_list = fields.Boolean('Use Price List')
    no_remove_commanded = fields.Boolean('Do Not Remove Commanded',
        help="Do not remove product commanded in pos")
    delivery_product = fields.Many2One('product.product', 'Delivery Product',
        domain=[
            ('template.salable', '=', True),
            ('template.type', '=', 'service'),
        ])
    allow_discount_handle = fields.Boolean('Allow Discount Handle')
    print_lines_product = fields.Boolean('Print Lines Product', help='Check if you do not want print product lines')
    exclude_tip_and_delivery = fields.Boolean('Exclude Tip and Delivery Product')
    advance_account = fields.Many2One('account.account', 'Advance Account',
        domain=[
            ('type', '!=', None),
        ])
    print_invoice_short = fields.Boolean('Invoice Short')
    print_invoice_payment = fields.Boolean('Print Invoice Payment')
    uvt_pos = fields.Float('Amount Uvt', help='Set amount uvt for restrict sell invoices pos')
    invoice_date_from_statement = fields.Boolean('Invoice Date From Statement')
    print_reference_receipt = fields.Boolean('Print Reference in Receip')

    # @classmethod
    # def __register__(cls, module_name):
    #     super(Configuration, cls).__register__(module_name)
    #     table = cls.__table_handler__(module_name)

    #     # Migration from 6.0.7: print invoice payment for remove after
    #     if table.column_exist('print_invoice_payment'):
    #         table.drop_column('print_invoice_payment')

    @staticmethod
    def default_show_stock_pos():
        return 'value'

    @staticmethod
    def default_discount_pos_method():
        return 'percentage'

    @staticmethod
    def default_reconcile_invoices():
        return True
