# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, Workflow, Unique
from trytond.pyson import Eval
from datetime import datetime, date
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import (
    Wizard, StateView, Button, StateTransition, StateAction)
from trytond.report import Report
from .exceptions import SaleDeviceError, StatementDraftError
from trytond.i18n import gettext
from trytond.modules.sale_pos.exceptions import PartyMissingAccount

STATES_DELIVERY = [
    ('', ''),
    ('to_dispatch', 'To Dispatch'),
    ('dispatched', 'Dispatched'),
    ('delivered', 'Delivered'),
    ('canceled', 'Canceled'),
]

STATES = {
    'readonly': Eval('state').in_(['dispatched', 'dispatched']),
}

TYPE_VEHICLE = [
    ('', ''),
    ('motorcycle', 'Motorcycle'),
    ('bicycle', 'Bicycle'),
    ('car', 'Car'),
]


class DeliveryParty(ModelSQL, ModelView):
    "Delivery Party"
    __name__ = "sale.delivery_party"
    active = fields.Boolean('Active')
    party = fields.Many2One('party.party', 'Party', required=True)
    number_plate = fields.Char('Number Plate')
    type_vehicle = fields.Selection(TYPE_VEHICLE, 'Type Vehicle')

    def get_rec_name(self, name):
        party = self.party.name if self.party else ''
        return party

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('party.rec_name',) + tuple(clause[1:])]


class SaleRelationDeliveryParty(Workflow, ModelSQL, ModelView):
    'Sale Relation Delivery Party'
    __name__ = 'sale.sale.delivery_party'
    delivery_party = fields.Many2One('sale.delivery_party', 'Delivery Men',
        required=True, states=STATES)
    sale = fields.Many2One('sale.sale', 'Sale', required=True, states=STATES)
    dispatch_date = fields.DateTime('Dispatch Date', readonly=True)
    delivery_date = fields.DateTime('Delivery Date', readonly=True)
    state = fields.Selection(STATES_DELIVERY, 'State', required=True,
        readonly=True)
    residual_amount = fields.Function(fields.Numeric('Residual Amount',
        digits=(16, 2), readonly=True), 'get_sale_fields')
    paid_amount = fields.Function(fields.Numeric('Paid Amount', digits=(16, 2),
        readonly=True), 'get_sale_fields')
    # channel = fields.Function(fields.Many2One('sale.web_channel', 'Channel',
    #     readonly=True), 'get_sale_fields')

    @classmethod
    def __setup__(cls):
        super(SaleRelationDeliveryParty, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
               ('sale_uniq', Unique(table, table.sale),
                   'Sale already exists in other dispatch!'),
            ]
        cls._transitions |= set((
            ('to_dispatch', 'dispatched'),
            ('to_dispatch', 'canceled'),
            ('canceled', 'to_dispatch'),
            ('dispatched', 'canceled'),
            ('dispatched', 'to_dispatch'),
            ('dispatched', 'delivered'),
            ('delivered', 'dispatched'),
            ))
        cls._buttons.update({
            'to_dispatch': {
                'invisible': Eval('state') != 'canceled',
            },
            'dispatched': {
                'invisible': Eval('state') != 'to_dispatch',
            },
            'delivered': {
                'invisible': Eval('state') != 'dispatched',
            },
            'cancel': {
                'invisible': Eval('state').in_(['canceled', 'delivered']),
            },
        })

    @staticmethod
    def default_state():
        return 'to_dispatch'

    def get_sale_fields(self, name=None):
        if name == 'paid_amount':
            return self.sale.paid_amount
        if name == 'residual_amount':
            return self.sale.residual_amount

    @classmethod
    @ModelView.button
    @Workflow.transition('to_dispatch')
    def to_dispatch(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('dispatched')
    def dispatched(cls, records):
        for rec in records:
            if not rec.dispatch_date:
                rec.dispatch_date = datetime.now()
                rec.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('delivered')
    def delivered(cls, records):
        for rec in records:
            if not rec.delivery_date:
                rec.delivery_date = datetime.now()
                rec.save()

    def get_rec_name(self, name):
        sale_ = self.sale.number if self.sale else ''
        delivery = self.delivery_party._rec_name if self.delivery_party else ''
        self._rec_name = delivery + '[' + sale_ + ']'
        return (self._rec_name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('sale.number',) + tuple(clause[1:]),
                ('delivery_party.party',) + tuple(clause[1:]),
                ]


class PaySalesDeliveryForm(ModelView):
    'Pay Sales Delivery Form'
    __name__ = 'sale_pos_frontend.pay_sales_delivery_form'
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
        domain=[
            ('id', 'in', Eval('journals', [])),
            ('require_voucher', '=', False),
        ], depends=['journals'], required=True)
    journals = fields.One2Many('account.statement.journal', None,
        'Allowed Statement Journals')
    amount_to_pay = fields.Numeric('Amount', readonly=True)


class PaySalesDelivery(Wizard):
    'Pay Sales Delivery'
    __name__ = 'sale_pos_frontend.pay_sales_delivery'
    start = StateView(
        'sale_pos_frontend.pay_sales_delivery_form',
        'sale_pos_frontend.pay_sales_delivery_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PaySalesDelivery, cls).__setup__()

    def default_start(self, fields):
        pool = Pool()
        SaleDelivery = pool.get('sale.sale.delivery_party')
        User = pool.get('res.user')
        sales_delivery = SaleDelivery.browse(Transaction().context['active_ids'])
        user = User(Transaction().user)
        sale_device = user.sale_device or False
        if user.id != 0 and not sale_device:
            raise SaleDeviceError(
                gettext('sale_pos_frontend.msg_not_sale_device'))
        return {
            'amount_to_pay': sum([sd.residual_amount or 0
                                  for sd in sales_delivery]),
            'journal': sale_device.journal.id
                if sale_device.journal else None,
            'journals': [j.id for j in sale_device.journals],
        }

    def transition_pay_(self):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        Sale = pool.get('sale.sale')
        SaleDeliveryParty = pool.get('sale.sale.delivery_party')
        Statement = pool.get('account.statement')
        StatementLine = pool.get('account.statement.line')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        sales_delivery_party = SaleDeliveryParty.browse(ids)

        form = self.start
        for sd in sales_delivery_party:
            sale = sd.sale
            device_id = user.sale_device.id if user.sale_device else sale.sale_device.id
            statements = Statement.search([
                    ('journal', '=', form.journal),
                    ('state', '=', 'draft'),
                    ('sale_device', '=', device_id),
                ], order=[('date', 'DESC')])
            if not statements:
                raise StatementDraftError(
                    gettext('sale_pos_frontend.msg_not_draft_statement', s=form.journal.name))

            if not sale.number:
                Sale.set_number([sale])

            if not sale.party.account_receivable:
                raise PartyMissingAccount(
                    gettext('sale_shop.msg_party_without_account_receivable', s=sale.party.name))
            account = sale.party.account_receivable.id
            desc = sale.invoices[0].number + ' [ ' + sale.number + ' ]' if sale.invoices else sale.invoice_number
            payment = StatementLine(
                statement=statements[0].id,
                date=date.today(),
                amount=sale.residual_amount,
                party=sale.party.id,
                account=account,
                description=desc,
                sale=sale.id,
            )
            payment.save()

            if sale.total_amount != sale.paid_amount:
                return 'start'
            sale.save()
            sd.state = 'delivered'
            sd.save()
            for inv in sale.invoices:
                if inv.state == 'posted':
                    inv.write([inv], {'state': 'draft'})
            Sale.workflow_to_end([sale])
        return 'end'


class SaleDetailedDeliveryManStart(ModelView):
    'Sale Detailed Delivery Man Start'
    __name__ = 'sale_pos_frontend.detailed_deliveryman.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True, domain=[
        ('company', '=', Eval('company'))
    ])
    sale_date = fields.Date('Sale Date', required=True)
    delivery_parties = fields.Many2Many('sale.delivery_party', None, None,
        'Delivery Party')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_shop():
        return Transaction().context.get('shop')

    @staticmethod
    def default_sale_date():
        today_ = date.today()
        return today_


class SaleDetailedDeliveryMan(Wizard):
    'Sale Detailed Delivery'
    __name__ = 'sale_pos_frontend.detailed_deliveryman'
    start = StateView(
        'sale_pos_frontend.detailed_deliveryman.start',
        'sale_pos_frontend.detailed_deliveryman_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateAction('sale_pos_frontend.detailed_deliveryman_report')

    def do_print_(self, action):
        data = {
            'company_name': self.start.company.party.name,
            'shop': self.start.shop.id,
            'shop_name': self.start.shop.name,
            'sale_date': self.start.sale_date,
            }
        if self.start.delivery_parties:
            data['delivery_parties'] = [
                dp.id for dp in self.start.delivery_parties]
        return action, data

    def transition_print_(self):
        return 'end'


class SaleDetailedDeliveryManReport(Report):
    __name__ = 'sale_pos_frontend.detailed_deliveryman_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        object = {}
        sum_paid = []
        sum_residual = []
        domain = [
            ('sale_date', '=', data['sale_date']),
            ('shop', '=', data['shop']),
            ('delivery_party', '!=', None),
            ('number', '!=', None),
        ]
        if data.get('delivery_parties'):
            domain.append(
                ('delivery_party', 'in', data['delivery_parties'])
            )

        cont = 0
        sales = Sale.search(domain)
        cash_received = 0
        for sd in sales:
            dp_id = sd.delivery_party
            if dp_id not in object.keys():
                object[dp_id] = {
                    'delivery_man': sd.delivery_party.party.name,
                    'sales': [],
                    'total_pay_amount': [],
                    'total_delivery': [],
                    'total_residual_amount': [],
                }
            object[dp_id]['sales'].append(sd)
            if sd.cash_received:
                cash_received = sd.cash_received
            if sd.delivery:
                object[dp_id]['total_delivery'].append(sd.delivery)

            object[dp_id]['total_residual_amount'].append(
                sd.total_amount_cache - cash_received
            )
            object[dp_id]['total_pay_amount'].append(cash_received)
            sum_paid.append(cash_received)
            sum_residual.append(sd.total_amount_cache - cash_received)
            cash_received = 0
            cont += 1
        report_context['records'] = object.values()
        report_context['sale_date'] = data['sale_date']
        report_context['cont'] = cont
        report_context['company'] = data['company_name']
        report_context['shop'] = data['shop_name']
        report_context['sum_paid'] = sum(sum_paid)
        report_context['sum_residual'] = sum(sum_residual)
        return report_context
