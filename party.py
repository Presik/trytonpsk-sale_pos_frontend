# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Party(metaclass=PoolMeta):
    __name__ = "party.party"
    categories_string = fields.Function(
        fields.Char('Categories String'), 'get_categories_string'
    )

    def get_categories_string(self, name):
        _text = ''
        for cat in self.categories:
            _text += cat.name + ' '
        return _text
