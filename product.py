# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from sql import Table
from sql import Window
from sql.functions import RowNumber
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'
    name_icon = fields.Selection([
        ('', ''),
        ('beer', 'beer'),
        ('breakfast', 'breakfast'),
        ('chicken_leg', 'chicken-leg'),
        ('chinese_food', 'chinese-food'),
        ('burger', 'burger'),
        ('donut', 'donut'),
        ('coffee', 'coffee'),
        ('hot_dog', 'hot-dog'),
        ('ice_cream', 'ice-cream'),
        ('taco', 'taco'),
        ('tea', 'tea'),
        ('waffle', 'waffle'),
        ('pizza', 'pizza'),
        ('pizza-slice', 'pizza-slice'),
        ('croissant', 'croissant'),
        ('waiter', 'waiter'),
        ('kebab', 'kebab'),
        ('milkshake', 'milkshake'),
        ('spaghetti', 'spaghetti'),
        ('cheese', 'cheese'),
        ('lunch', 'lunch'),
        ('dessert', 'Dessert'),
        ('sausage', 'Sausage'),
        ('rice', 'rice'),
        ('french_fries', 'french-fries'),
        ('breakfast', 'breakfast'),
        ('sushi', 'sushi'),
        ('juice', 'juice'),
    ], 'Name Icon', select=True)
    products = fields.Many2Many('product.product-product.category', 'category', 'product', 'Products')


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    short_name = fields.Char('Short Name')

    @classmethod
    def sync_get_products(cls, args, context=None):
        write_date = args.get('write_date')
        shop_id = args.get('shop_id')
        cursor = Transaction().connection.cursor()
        categories = []
        if shop_id:
            Shop = Pool().get('sale.shop')
            shop, = Shop.search([
                ('id', '=', shop_id)
            ])
            categories = [str(cat.id) for cat in shop.product_categories]
            categories_string = ",".join(categories)

        if categories:
            cursor.execute("SELECT pp.id, pp.write_date, pp.code, pp.barcode, pt.name, \
                pp.description, pt.account_category, pt.sale_price_w_tax::VARCHAR \
                FROM product_product AS pp \
                INNER JOIN product_template AS pt ON pp.template=pt.id \
                WHERE pp.code IS NOT NULL AND pt.active=true AND pp.active=true AND pt.salable=true AND \
                    pt.account_category in (%s) AND \
                        (pt.write_date>='%s' OR pt.create_date>='%s') \
                ORDER BY id ASC" % (categories_string, write_date, write_date)
            )
        else:
            cursor.execute(
                "SELECT pp.id, pp.write_date, pp.code, pp.barcode, pt.name, \
                    pp.description, pt.account_category, pt.sale_price_w_tax::VARCHAR \
                FROM product_product AS pp \
                INNER JOIN product_template AS pt ON pp.template=pt.id \
                WHERE pp.code IS NOT NULL AND pt.active=true AND pp.active=true AND pt.salable=true AND \
                    pt.account_category IS NOT NULL AND \
                        (pt.write_date>='%s' OR pt.create_date>='%s') \
                ORDER BY id ASC" % (write_date, write_date))
        res = cursor.fetchall()
        return res

    @classmethod
    def get_update_product_cost_fix(cls):
        cursor = Transaction().connection.cursor()
        query = """select pp.id, pcp.unit_price, pcp.unit, pt.default_uom,  pt.purchase_uom
        from product_product as pp
        left join product_template as pt on pp.template = pt.id
        left join product_cost_price as pc on pp.id =pc.product
        right join (select distinct on(product) product, unit_price, unit, create_date from account_invoice_line where invoice in (select id from account_invoice where type='in' and state in('posted', 'paid')) and product in (select pp.id
        from product_product as pp
        left join product_template as pt on pp.template=pt.id
        left join product_cost_price as pc on pp.id =pc.product
        where pc.cost_price=0 and pt.purchasable='t' and pt.type='goods') order by product, create_date desc) as pcp on pp.id=pcp.product
        where pc.cost_price=0 and pt.purchasable='t' and pt.type='goods'"""
        cursor.execute(query)
        res = cursor.fetchall()
        for row in res:
            cls.write(list(cls.browse([row[0]])), {'cost_price': row[1]})

    @classmethod
    def get_stock_by_locations(cls, args, context=None):
        Location = Pool().get('stock.location')
        code = args.get('code')
        products = cls.search([
            ('code', '=', str(code)),
        ])
        if not products:
            return []

        product = products[0]

        ctx = {
            'stock_date_end': date.today(),
            'product': product.id,
            'active_test': False,
        }

        with Transaction().set_context(ctx):
            locations = Location.search([
                ('type', '=', 'warehouse'),
                ('active', '=', True)
            ])
            res = [(l.name, str(l.quantity)) for l in locations]
            return res


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    # sale_price_w_tax = fields.Numeric('Sale Price With Tax', digits=(16, 2),
    #     depends=['list_price', 'account_category'])
    printers = fields.Many2Many('product.template-sale.pos_printer',
        'template', 'printer', 'Printers')
    printable_order = fields.Boolean('Printable Order')

    min_margin = fields.Float('Minimal Margin', digits=(4, 2))
    standard_margin = fields.Float('Standard Margin', digits=(4, 2))


class UpdateSalePriceWTax(Wizard):
    'Update Sale Price With Tax'
    __name__ = 'product.update_price'
    start_state = 'update_price'
    update_price = StateTransition()

    def transition_update_price(self):
        Template = Pool().get('product.template')
        products = Template.search([])
        for p in products:
            Template.write([p], {
                'sale_price_w_tax': p.compute_list_price_w_tax(p.list_price)
            })
        return 'end'


class ProductPrinter(ModelSQL):
    "Product Printer"
    __name__ = "product.template-sale.pos_printer"
    _table = 'product_template_sale_pos_printer_rel'
    template = fields.Many2One('product.template', 'Product',
        ondelete='CASCADE', select=True, required=True)
    printer = fields.Many2One('sale.pos_printer', 'Printer',
        ondelete='RESTRICT', select=True, required=True)


class AddPrintersStart(ModelView):
    'Add Printers Start'
    __name__ = 'product.add_printers.start'
    printers = fields.Many2Many('sale.pos_printer', None, None,
            'Printers')


class AddPrinters(Wizard):
    'Add Printers'
    __name__ = 'product.add_printers'
    start = StateView('product.add_printers.start',
        'sale_pos_frontend.add_printers_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add', 'add', 'tryton-ok', default=True),
        ])
    add = StateTransition()

    def transition_add(self):
        pool = Pool()

        Template = pool.get('product.template')
        templates_ids = Transaction().context.get('active_ids')
        if templates_ids:
            templates = Template.browse(templates_ids)
            Template.write(templates, {'printers': [('add', self.start.printers)]})
        return 'end'


class CategoryProduct(ModelSQL):
    "Product - Category"
    __name__ = 'product.product-product.category'

    product = fields.Many2One('product.product', 'Template')
    category = fields.Many2One('product.category', 'Category')

    @classmethod
    def table_query(cls):
        pool = Pool()
        Product = pool.get('product.product')
        product_template = Table('product_template-product_category')
        product = Product.__table__()
        window = Window(
                [],
                order_by=[product_template.category.asc])
        return product.join(
            product_template,
            condition=product.template == product_template.template
            ).select(
            RowNumber(window=window).as_('id'),
            product.create_uid.as_('create_uid'),
            product.create_date.as_('create_date'),
            product.write_uid.as_('write_uid'),
            product.write_date.as_('write_date'),
            product.id.as_('product'),
            product_template.category.as_('category'),
            where=(product.active == True))
