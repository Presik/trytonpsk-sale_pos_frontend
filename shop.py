# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta

REPORTS = [
    ('sale_pos.shop_daily_category_report', 'Shop Daily Category Report'),
    ('sale_pos.shop_daily_summary_report', 'Shop Daily Summary Report'),
    ('sale_pos.sale_by_kind_report', 'Sale By Kind Report'),
    ('sale_pos_frontend.sale_square_box_report', 'Sale Square Box Report')
]


class SaleDiscont(ModelSQL, ModelView):
    "Sale Discount"
    __name__ = "sale.discount"
    active = fields.Boolean('Active')
    discount = fields.Numeric('Discount', digits=(16, 2), required=True)
    name = fields.Char('Name', required=True)
    type_discount = fields.Selection([
            ('percentage', 'percentage'),
            ('fixed', 'fixed'),
            ], 'Type Discount')

    @classmethod
    def __setup__(cls):
        super(SaleDiscont, cls).__setup__()

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_type_discount():
        return 'percentage'

    def get_rec_name(self, name):
        return self.name + ' [ ' + str(self.discount) + ' ]'


class SaleShop(metaclass=PoolMeta):
    __name__ = 'sale.shop'
    # Field to use for legal information of Goverment Tax Administration
    gta_info = fields.Char('Gta Info')
    ticket_header = fields.Text('Ticket Header')
    ticket_footer = fields.Text('Ticket Footer')
    salesman_pos_required = fields.Boolean('Salesman POS Required')
    turn_required = fields.Boolean('Turn Required')
    invoice_copies = fields.Integer('Invoice Copies')
    order_copies = fields.Integer('Order Copies')
    discount_pos_method = fields.Selection([
        ('percentage', 'Percentage'),
        ('fixed', 'Fixed'),
        ], 'Discount POS Method', required=True)
    show_position = fields.Boolean('Show Customer Position')
    product_categories = fields.Many2Many('sale.shop-product.category',
        'shop', 'category', 'Categories')
    taxes = fields.Many2Many('sale.shop-account.tax', 'shop', 'tax', 'Taxes',
        domain=[
            ('group.kind', '=', 'sale')
        ])
    delivery_man = fields.Many2Many('sale.shop.delivery_party', 'shop',
                                    'delivery_men', 'Delivery Man')
    discounts = fields.Many2Many('sale.shop.discounts', 'shop',
                                 'discount', 'Discounts')
    reports = fields.MultiSelection(REPORTS, 'Reports')

    @staticmethod
    def default_invoice_copies():
        return 1

    @staticmethod
    def default_discount_pos_method():
        return 'fixed'

    def get_authorizations(self):
        res = {}
        auths = []
        if self.pos_authorization:
            auths.append(self.pos_authorization)
        if self.electronic_authorization:
            auths.append(self.electronic_authorization)
        if self.credit_note_electronic_authorization:
            auths.append(self.credit_note_electronic_authorization)

        for auth in auths:
            res[auth.kind] = self.get_auth_data(auth)
        return res

    def get_auth_data(self, auth):
        res = {}
        if auth:
            res = {
                'number': auth.number,
                'start_date_auth': auth.start_date_auth,
                'end_date_auth': auth.end_date_auth,
                'from_auth': auth.from_auth,
                'to_auth': auth.to_auth,
                'kind': auth.kind_string,
                'prefix': auth.sequence.prefix
            }
        return res


class SaleShopDiscounts(ModelSQL):
    'Sale Shop - Discounts'
    __name__ = 'sale.shop.discounts'
    _table = 'sale_shop_discounts'

    shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
        required=True)
    discount = fields.Many2One('sale.discount', 'Discount',
        ondelete='RESTRICT', required=True)


class SaleShopDeliveryMen(ModelSQL):
    'Sale Shop - Delivery Men'
    __name__ = 'sale.shop.delivery_party'
    _table = 'sale_shop_delivery_party'

    shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
        required=True)
    delivery_men = fields.Many2One('sale.delivery_party',
        'Delivery Men', ondelete='RESTRICT', required=True)


class ShopProductCategory(ModelSQL):
    'Shop - Category'
    __name__ = 'sale.shop-product.category'
    shop = fields.Many2One('sale.shop', 'Shop',
        ondelete='CASCADE', required=True)
    category = fields.Many2One('product.category', 'Category',
        ondelete='CASCADE', required=True)


class ShopTaxes(ModelSQL):
    'Shop - Taxes'
    __name__ = 'sale.shop-account.tax'
    shop = fields.Many2One('sale.shop', 'Shop',
        ondelete='CASCADE', required=True)
    tax = fields.Many2One('account.tax', 'Taxes',
        ondelete='CASCADE', required=True)
