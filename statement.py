# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql import Table
from trytond.transaction import Transaction
from trytond.model import fields, ModelView
from trytond.pool import Pool, PoolMeta
from trytond.wizard import StateTransition, Wizard, Button, StateView
from trytond.pyson import Eval
from datetime import date


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'

    @classmethod
    def multipayment_invoices(cls, args, ctx=None):
        """
        args dict of
            {
                'sale_ids' : [] , required list of ids for process pay
                'statement_id' : integer, required statement to add pay
            }
        """
        pool = Pool()
        Date = pool.get('ir.date')
        Sale = pool.get('sale.sale')
        StatementLine = pool.get('account.statement.line')
        Statement = pool.get('account.statement')
        if args.get('sale_ids'):
            sales_ids = args.get('sale_ids')
        else:
            sales_ids = args.get('sales_ids')

        sales = Sale.browse(sales_ids)
        if args.get('statement_id'):
            statement_id = args['statement_id']
        else:
            sale_device_id = sales[0].sale_device.id
            statement, = Statement.search([
                ('sale_device', '=', sale_device_id),
                ('journal', '=', args['journal_id']),
                ('state', '=', 'draft'),
            ], limit=1)
            statement_id = statement.id

        for sale in sales:
            if not sale.number:
                continue
            if sale.payments:
                total_paid = sum([p.amount for p in sale.payments])
                if total_paid >= sale.total_amount:
                    continue
            Sale.post_invoices(sale)
            Sale.do_stock_moves([sale])
            account_id = sale.party.account_receivable.id

            # We need to check before if current statement is in draft, because
            # this could be closed (validated), in this case it should
            # not accept any payment and must be return more with a specific
            # error message
            # FIXME: Wilson Add more sprecific error in a modal
            _statement = Statement(statement_id)
            if _statement.state != 'draft':
                return 'error'

            to_create = {
                'sale': sale.id,
                'date': Date.today(),
                'statement': statement_id,
                'amount': sale.total_amount,
                'party': sale.party.id,
                'account': account_id,
                'description': sale.invoice_number or sale.invoice.number or '',
            }
            line, = StatementLine.create([to_create])
            turn = line.statement.turn
            sale_device = line.statement.sale_device
            sale.write([sale], {
                'turn': turn,
                'order_status': 'delivered',
                'sale_device': sale_device,
            })
            invoice = sale.invoice
            invoice.turn = turn
            invoice.save()
            Sale.do_reconcile([sale])
        return 'ok'

    @classmethod
    def apply_payment_collection(cls, args):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        configuration = pool.get('sale.configuration')(1)
        if not configuration.advance_account:
            return {
                'status': 'error',
                'message': 'configure account advance in configuration of sales'}
        account_id = configuration.advance_account.id
        to_create = {
            'date': date.today(),
            'statement': args.get('statement'),
            'amount': args.get('amount'),
            'party': args['party'],
            'number': args['voucher_number'],
            'account': account_id,
        }
        line, = StatementLine.create([to_create])
        line.create_move()
        return {
            'status': 'ok',
            'message': 'pago aplicado exitosamente...!'
        }


class StatementForceDraft(Wizard):
    'Statement Force Draft'
    __name__ = 'account.statement.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        account_statement = Table('account_statement')
        id_ = Transaction().context['active_id']
        cursor = Transaction().connection.cursor()
        if id_:
            cursor.execute(*account_statement.update(
                columns=[account_statement.state],
                values=['draft'],
                where=account_statement.id == id_)
            )
        return 'end'


class MultiPaymentDeliveryStart(ModelView):
    'MultiPayment Delivery Start'
    __name__ = 'sale_pos_frontend.multipayment_delivery.start'
    # company = fields.Many2One('company.company', 'Company', required=True)
    # shop = fields.Many2One('sale.shop', 'Shop', required=True)
    delivery_party = fields.Many2One('sale.delivery_party', 'Delivery Party',
        required=True)
    # statement = fields.Many2One('account.statement', 'Statement', domain=[
    #     ('state', '=', 'draft'),
    #     ('shop', '=', Eval('shop')),
    # ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    # @staticmethod
    # def default_shop():
    #     return Transaction().context.get('shop')


class MultiPaymentDeliveryPay(ModelView):
    'MultiPayment Delivery Pay'
    __name__ = 'sale_pos_frontend.multipayment_delivery.pay'
    delivery_party = fields.Many2One('sale.delivery_party',
        'Delivery Party', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    sales = fields.Many2Many('sale.sale', None, None, 'Sales', domain=[
        ('consumer', '=', Eval('delivery_party')),
        ('shop', '=', Eval('shop')),
    ])
    total_amount = fields.Function(fields.Numeric('Total Amount',
        digits=(16, 2), depends=['sales']),
        'on_change_with_total_amount')

    @staticmethod
    def default_shop():
        return Transaction().context.get('shop')

    @fields.depends('sales')
    def on_change_with_total_amount(self, name=None):
        if self.sales:
            return sum([s.total_amount for s in self.sales])
        return 0


class MultiPaymentDelivery(Wizard):
    'MultiPayment Delivery'
    __name__ = 'sale_pos_frontend.multipayment_delivery'
    start = StateView(
        'sale_pos_frontend.multipayment_delivery.start',
        'sale_pos_frontend.statement_multipayment_delivery_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'search', 'tryton-ok', default=True),
        ])
    search = StateTransition()
    pay_ = StateView(
        'sale_pos_frontend.multipayment_delivery.pay',
        'sale_pos_frontend.statement_multipayment_delivery_pay_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Process', 'process', 'tryton-ok', default=True),
        ])
    process = StateTransition()

    def transition_search(self):
        return 'pay_'

    def default_pay_(self, fields):
        pool = Pool()
        Statement = pool.get('account.statement')
        Sale = Pool().get('sale.sale')
        id_ = Transaction().context['active_id']
        statement = Statement(id_)
        sales = Sale.search([
            ('shop', '=', statement.sale_device.shop.id),
            ('state', 'not in', ['cancel', 'done']),
            ('kind', '=', 'delivery'),
            ('delivery_party', '=', self.start.delivery_party.id),
        ])
        sale_ids = [s.id for s in sales]
        return {
            'sale_ids': sale_ids,
            'delivery_party': self.start.delivery_party.id,
        }

    def transition_process(self):
        Statement = Pool().get('account.statement')
        id_ = Transaction().context['active_id']
        args = {
            'statement_id': id_,
            'sale_ids': self.pay_.sale_ids,
        }
        Statement.multipayment_invoices(args)
        return 'end'
